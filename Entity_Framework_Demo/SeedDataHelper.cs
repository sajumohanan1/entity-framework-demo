﻿using Entity_Framework_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework_Demo
{
    public class SeedDataHelper
    {
        public static List<Professor> GetProfessors()
        {
            //Populate list
            List<Professor> professors = new List<Professor>()
            {
                new Professor() { Id=1, FirstName="Albert", LastName="George", Subject="IT"},
                new Professor() { Id=2, FirstName="David", LastName="Mike", Subject="Business"},
                new Professor() { Id=3, FirstName="Daniel", LastName="Joseph", Subject="Engg"}                
            };            
            return professors;
        }

        
        public static List<ResearchProject> GetResearchProjects()
        {
            List<ResearchProject> projects = new List<ResearchProject>()
            {
                new ResearchProject() { Id = 1, Title = "IT Project", StudentID=1 },
                new ResearchProject() { Id = 2, Title = "Business", StudentID=2 }
            };            
            return projects;
        }
        

        public static List<Student> GetStudents()
        {
            List<Student> students = new List<Student>()
            {
                new Student() { Id = 1, FirstName = "Leen", LastName = "Gillis", Age = 25, ProfessorId=1 },
                new Student() { Id = 2, FirstName = "Stine", LastName = "Wilmer", Age = 30, ProfessorId=2 },
                new Student() { Id = 3, FirstName = "Aina", LastName = "M", Age = 22, ProfessorId=3 }
            };
            return students;
        }

    }
}
