﻿// <auto-generated />
using Entity_Framework_Demo;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Entity_Framework_Demo.Migrations
{
    [DbContext(typeof(UniversityDBContext))]
    [Migration("20230122220300_add-ManyToMany-Professor-Qualification")]
    partial class addManyToManyProfessorQualification
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("Entity_Framework_Demo.Models.Professor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Subject")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Professors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FirstName = "Albert",
                            LastName = "George",
                            Subject = "IT"
                        },
                        new
                        {
                            Id = 2,
                            FirstName = "David",
                            LastName = "Mike",
                            Subject = "Business"
                        },
                        new
                        {
                            Id = 3,
                            FirstName = "Daniel",
                            LastName = "Joseph",
                            Subject = "Engg"
                        });
                });

            modelBuilder.Entity("Entity_Framework_Demo.Models.Qualification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<string>("Degree")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.HasKey("Id");

                    b.ToTable("Qualifications");
                });

            modelBuilder.Entity("Entity_Framework_Demo.Models.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .UseIdentityColumn();

                    b.Property<int>("Age")
                        .HasColumnType("int");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProfessorId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ProfessorId");

                    b.ToTable("Students");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Age = 25,
                            FirstName = "Leen",
                            LastName = "Gillis",
                            ProfessorId = 1
                        },
                        new
                        {
                            Id = 2,
                            Age = 30,
                            FirstName = "Stine",
                            LastName = "Wilmer",
                            ProfessorId = 2
                        },
                        new
                        {
                            Id = 3,
                            Age = 22,
                            FirstName = "Aina",
                            LastName = "M",
                            ProfessorId = 3
                        });
                });

            modelBuilder.Entity("ProfessorQualification", b =>
                {
                    b.Property<int>("ProfessorsId")
                        .HasColumnType("int");

                    b.Property<int>("QualificationsId")
                        .HasColumnType("int");

                    b.HasKey("ProfessorsId", "QualificationsId");

                    b.HasIndex("QualificationsId");

                    b.ToTable("ProfessorQualification");
                });

            modelBuilder.Entity("Entity_Framework_Demo.Models.Student", b =>
                {
                    b.HasOne("Entity_Framework_Demo.Models.Professor", "Professor")
                        .WithMany()
                        .HasForeignKey("ProfessorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Professor");
                });

            modelBuilder.Entity("ProfessorQualification", b =>
                {
                    b.HasOne("Entity_Framework_Demo.Models.Professor", null)
                        .WithMany()
                        .HasForeignKey("ProfessorsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Entity_Framework_Demo.Models.Qualification", null)
                        .WithMany()
                        .HasForeignKey("QualificationsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
