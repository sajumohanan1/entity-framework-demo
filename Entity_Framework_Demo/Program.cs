﻿using Entity_Framework_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Entity_Framework_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Student addStudent = new Student() { Id = 4, FirstName = "FirstName", LastName = "LastName", Age = 35, ProfessorId=1 };
            //AddStudent(addStudent);

            //DeleteStudent(3);

            //AddStudent(new Student() { FirstName="David", LastName="Joseph", Age=40, ProfessorId=3});

           // Student modifyStudent = new Student() { Id= 1, FirstName="Marry", LastName="Nik", Age=26 };//Set values
           // UpdateStudent(modifyStudent); //call the update method


            List<Professor> professors = GetProfessors(); 
            List<Student> students = GetStudents();
            List<ResearchProject> projects = GetStudentProjects();

            foreach (var item in students)
            {
                Console.WriteLine(item.Id + " " + item.FirstName + " " + item.LastName + " " + item.Age + " " + item.ProfessorId);
            }

            Console.WriteLine("---------------------------------------------");

            foreach (var item in professors)
            {
                Console.WriteLine(item.Id + " " + item.FirstName + " " + item.LastName + " " + item.Subject);
            }

            Console.WriteLine("---------------------------------------------");

            foreach (var item in projects)
            {
                Console.WriteLine(item.Id + " " + item.Title + " " + item.StudentID);
            }

            /*
            Add new packages for the dependencies
            Add models (Set properties for the data models)
            Add DbContext [DbSets, ConnectionString]
            Migration using PM Console + Check Migration
            Update Database
            */


        }
        //Get data (Record) - Professor
        public static List<Professor> GetProfessors()
        {
            List<Professor> professors = new List<Professor>();
            //Instantiate a new instance into the DbContext
            UniversityDBContext db = new UniversityDBContext();
            professors = db.Professors.ToList();            
            return professors;
        }

        //Get data(Record) - Student
        public static List<Student> GetStudents()
        {
            List<Student> students = new List<Student>();
            UniversityDBContext db = new UniversityDBContext();
            students = db.Students.ToList();
            return students;
        }

        //Get data(Record) - Research Project
        public static List<ResearchProject> GetStudentProjects()
        {
            List<ResearchProject> projects = new List<ResearchProject>();
            UniversityDBContext db = new UniversityDBContext();
            projects = db.ResearchProjects.ToList();
            return projects;
        }

        //Add data (Record) - Add Student
        public static void AddStudent(Student student)
        {
            UniversityDBContext dBContext = new UniversityDBContext();
            dBContext.Students.Add(student);            
            dBContext.SaveChanges();
        }

        //Upate data
        public static void UpdateStudent(Student student)
        {
            UniversityDBContext dBContext = new UniversityDBContext();
            //Get the record by ID for the update, use Find method.
            //Get the ID and pull it into the database
            Student studentToUpdate=dBContext.Students.Find(student.Id);
            //Perform modify for each column
            studentToUpdate.FirstName=student.FirstName;
            studentToUpdate.LastName=student.LastName;
            studentToUpdate.Age=student.Age;            
            dBContext.SaveChanges();
        }

        //Delete Data (Record)
        //Perform delete againt the PK value of a table
        public static void DeleteStudent(int id)
        {
            UniversityDBContext dBContext = new UniversityDBContext();
            Student student=dBContext.Students.Find(id);
            dBContext.Students.Remove(student);
            dBContext.SaveChanges();
        }
    }
}
