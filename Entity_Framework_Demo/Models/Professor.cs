﻿using System.Collections.Generic;

namespace Entity_Framework_Demo.Models
{
    public class Professor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Subject { get; set; }

        public ICollection<Qualification> Qualifications { get; set; } //Collection navigation property

        

    }
}