﻿using System;

namespace Entity_Framework_Demo.Models
{
    public class ResearchProject
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public int StudentID { get; set; } //Foreign Key
        public Student student { get; set; } //Navigation property added to the Student class
    }
}