﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework_Demo.Models
{
    public class Qualification
    {
        [Key] //Primary key - set a data annotation
        public int Id { get; set; }

        [MaxLength(20)] //Data Annotation
        public string Degree { get; set; }

        //Below given is the Collection Navigation Property
        public ICollection<Professor> Professors { get; set; }
    }
}
