﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework_Demo.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }   
                       
        public int ProfessorId { get; set; } //Foreign Key property
        

        //Navigation Properies are declared below.
       public ResearchProject Project { get; set; }
        public Professor Professor { get; set; }
       

    }
}
