﻿using Entity_Framework_Demo.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity_Framework_Demo
{
    public class UniversityDBContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<ResearchProject> ResearchProjects { get; set; }

        //public DbSet<ResearchProject> ResearchProjects { get; set; }

        //Create OnConfiguring Method
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());
        }

        //Create Connection String
        private string GetConnectionString()
        {
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "SAJU-MOHANAN-NO\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "UnivDB";
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Feed list of professors here usign HasData method
            //Push all the professors into the modelBuilder.Entity<Professor>
            //Seed professor data
            modelBuilder.Entity<Professor>().HasData(SeedDataHelper.GetProfessors());

            /*
            //Seed research project data
            modelBuilder.Entity<ResearchProject>().HasData(SeedDataHelper.GetResearchProjects());
            */

            //Seed student data
            modelBuilder.Entity<Student>().HasData(SeedDataHelper.GetStudents());

            //seed researchproject
            modelBuilder.Entity<ResearchProject>().HasData(SeedDataHelper.GetResearchProjects());
        }
    }
}
